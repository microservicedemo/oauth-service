package com.anton.oauth.oauthdemo.service;

import com.anton.oauth.oauthdemo.model.AuthUserDetail;
import com.anton.oauth.oauthdemo.model.User;
import com.anton.oauth.oauthdemo.repository.UserDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDetailRepository userDetailRepository;

	@Override
	public UserDetails loadUserByUsername(String input) {
		Optional<User> user = null;

		if (input.contains("@")) {
			user = userDetailRepository.findByEmail(input);
		} else {
			user = userDetailRepository.findByUsername(input);
		}


		user.orElseThrow(() -> new UsernameNotFoundException("Username or password wrong"));

		UserDetails userDetails = new AuthUserDetail(user.get());
		new AccountStatusUserDetailsChecker().check(userDetails);
		return userDetails;
	}

}
