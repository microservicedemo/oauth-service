package com.anton.oauth.oauthdemo.repository;

import com.anton.oauth.oauthdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDetailRepository extends JpaRepository<User,Integer> {


    Optional<User> findByUsername(String name);
    Optional<User> findByEmail(String name);

}
